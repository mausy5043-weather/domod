#!/usr/bin/env python3

"""Common functions for use by kam*.py scripts"""

import datetime as dt
import sqlite3 as s3
import sys

import numpy as np


def add_time_line(config):
  final_epoch = int(dt.datetime.now().timestamp())
  step_epoch = 10 * 60
  multi = 3600
  if config['timeframe'] == 'hour':
    multi = 3600
  if config['timeframe'] == 'day':
    multi = 3600 * 24
  if config['timeframe'] == 'month':
    multi = 3600 * 24 * 31
  if config['timeframe'] == 'year':
    multi = 3600 * 24 * 366
  start_epoch = int((final_epoch - (multi * config['period'])) / step_epoch) * step_epoch
  config['timeline'] = np.arange(start_epoch, final_epoch, step_epoch, dtype='int')
  return config


def build_arrays44(lbls, use_data, expo_data):
  """Use the input to build two arrays and return them.

     example input line : "2015-01; 329811; 0"  : YYYY-MM; T1; T2
     the list comes ordered by the first field
     the first line and last line can be inspected to find
     the first and last year in the dataset.
    """
  first_year = int(lbls[0].split('-')[0])
  last_year = int(lbls[-1].split('-')[0]) + 1
  num_years = last_year - first_year

  label_lists = [np.arange(first_year, last_year), np.arange(1, 13)]
  usage = np.zeros((num_years, 12))
  exprt = np.zeros((num_years, 12))

  for data_point in zip(lbls, use_data, expo_data):
    [year, month] = data_point[0].split('-')
    col_idx = int(month) - 1
    row_idx = int(year) - first_year
    usage[row_idx][col_idx] = data_point[1]
    exprt[row_idx][col_idx] = data_point[2]
  return label_lists, usage, exprt


def contract(arr1, arr2):
  """
  Add two arrays together.
  """
  size = max(len(arr1), len(arr2))
  rev_arr1 = np.zeros(size, dtype=float)
  rev_arr2 = np.zeros(size, dtype=float)
  for idx in range(0, len(arr1)):
    rev_arr1[idx] = arr1[::-1][idx]
  for idx in range(0, len(arr2)):
    rev_arr2[idx] = arr2[::-1][idx]
  result = np.sum([rev_arr1, rev_arr2], axis=0)
  return result[::-1]


def contract24(arr1, arr2):
  result = [[]] * 24
  for hr in range(0, 24):
    result[hr] = contract(arr1[hr], arr2[hr])

  return result


def distract(arr1, arr2):
  """
  Subtract two arrays.
  Note: order is important!
  """
  size = max(len(arr1), len(arr2))
  rev_arr1 = np.zeros(size, dtype=float)
  rev_arr2 = np.zeros(size, dtype=float)
  for idx in range(0, len(arr1)):
    rev_arr1[idx] = arr1[::-1][idx]
  for idx in range(0, len(arr2)):
    rev_arr2[idx] = arr2[::-1][idx]
  result = np.subtract(rev_arr1, rev_arr2)
  result[result < 0] = 0.0
  return result[::-1]


def distract24(arr1, arr2):
  result = [[]] * 24
  for hr in range(0, 24):
    result[hr] = distract(arr1[hr], arr2[hr])
  return result


def get_cli_params(expected_amount):
  """Check for presence of a CLI parameter."""
  if len(sys.argv) != (expected_amount + 1):
    print(f"{expected_amount} arguments expected, {len(sys.argv) - 1} received.")
    sys.exit(0)
  return sys.argv[1]


def get_historic_data(dicti, parameter=None, from_start_of_year=False, include_today=True, somma=False):
  """Fetch historic data from SQLITE3 database.

  :param
  dict: dict - containing settings
  parameter: str - columnname to be collected
  from_start_of_year: boolean - fetch data from start of year or not
  include_today: boolean - also fetch today's data
  somma: boolean - return sum of grouped data (True) OR return avg of grouped data (False; default)

  :returns
  ret_data: numpy list int - data returned
  ret_lbls: numpy list str - label texts returned
  """
  period = dicti['period']
  if from_start_of_year:
    interval = f"datetime(datetime(\'now\', \'-{period + 1} {dicti['timeframe']}\'), \'start of year\')"
  else:
    interval = f"datetime(\'now\', \'-{period + 1} {dicti['timeframe']}\')"
  if include_today:
    and_where_not_today = ''
  else:
    and_where_not_today = 'AND (sample_time <= datetime(\'now\', \'-1 day\'))'
  db_con = s3.connect(dicti['database'])
  with db_con:
    db_cur = db_con.cursor()
    db_cur.execute(f"SELECT sample_epoch, \
                     {parameter} \
                     FROM {dicti['table']} \
                     WHERE (sample_time >= {interval}) \
                        {and_where_not_today} \
                        AND stn_id = 1 \
                     ORDER BY sample_epoch ASC \
                     ;"
                   )
    db_data = db_cur.fetchall()

  data = np.array(db_data)

  # interpolate the data to monotonic 10minute intervals provided by dicti['timeline']
  ret_epoch, ret_intdata = interplate(dicti['timeline'],
                                      np.array(data[:, 0], dtype=int),
                                      np.array(data[:, 1], dtype=int)
                                      )

  # group the data by dicti['grouping']
  ret_lbls, ret_grpdata = group_data(ret_epoch, ret_intdata, dicti['grouping'], somma)

  ret_data = ret_grpdata
  return ret_data[-period:], ret_lbls[-period:]


def interplate(epochrng, epoch, data):
  """Interpolate the given data to a neat monotonic dataset with 10 minute intervals
  """
  datarng = np.interp(epochrng, epoch, data)
  return epochrng, datarng


def group_data(x_epochs, y_data, grouping, somma):
  """"""
  x_texts = np.array([dt.datetime.fromtimestamp(i).strftime(grouping) for i in x_epochs], dtype='str')
  unique_x_texts = np.array(sorted(set(x_texts)), dtype='str')
  returned_y_data = np.zeros(np.shape(unique_x_texts))

  for idx, ut in enumerate(unique_x_texts):
    indices = np.where(x_texts == ut)[0]
    if len(indices) > 1:
      if somma:
        y = np.sum(y_data[indices[0]:indices[-1]])
      else:
        y = np.mean(y_data[indices[0]:indices[-1]])
    else:
      y = y_data[-1]
    returned_y_data[idx] = y

  return unique_x_texts, returned_y_data

