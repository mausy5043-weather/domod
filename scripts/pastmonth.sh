#!/bin/bash

# query daily totals for a period of one month

pushd "${HOME}/domod" >/dev/null || exit 1

./scripts/again21.py -m
./scripts/upload.sh --upload

popd >/dev/null || exit
