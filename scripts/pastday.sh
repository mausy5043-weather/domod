#!/bin/bash

# query hourly totals for a period of two days

pushd "${HOME}/domod" >/dev/null || exit 1

./scripts/again21.py -d
./scripts/upload.sh --upload

popd >/dev/null || exit
