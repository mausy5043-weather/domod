#!/usr/bin/env python3

"""Create trendbargraphs for various periods of weatherdata."""

import os
from datetime import datetime as dt

# noinspection PyUnresolvedReferences
import againlib as kl
import matplotlib.pyplot as plt
import numpy as np

DATABASE = os.environ['HOME'] + "/.sqlite3/weatherdata.sqlite3"


def fetch_last_day():
  """
    ...
    """
  config = kl.add_time_line({'grouping': '%m-%d %Hh',
                             'period': 50,
                             'timeframe': 'hour',
                             'database': DATABASE,
                             'table': 'weather'
                             })
  temperature, data_lbls = kl.get_historic_data(config, parameter='temperature')
  pressure, data_lbls = kl.get_historic_data(config, parameter='pressure')
  humidity, data_lbls = kl.get_historic_data(config, parameter='humidity')
  solrad, data_lbls = kl.get_historic_data(config, parameter='solrad', somma=True)
  solrad = solrad / 1000
  return data_lbls, temperature, pressure, humidity, solrad


def fetch_last_month():
  """
    ...
    """
  config = kl.add_time_line({'grouping': '%m-%d',
                             'period': 50,
                             'timeframe': 'day',
                             'database': DATABASE,
                             'table': 'weather'
                             })
  temperature, data_lbls = kl.get_historic_data(config, parameter='temperature')
  pressure, data_lbls = kl.get_historic_data(config, parameter='pressure')
  humidity, data_lbls = kl.get_historic_data(config, parameter='humidity')
  solrad, data_lbls = kl.get_historic_data(config, parameter='solrad', somma=True)
  solrad = solrad / 1000
  return data_lbls, temperature, pressure, humidity, solrad


def plot_graph(output_file, data_tuple, plot_title, show_data=False):
  """
    ...
    """
  data_lbls = data_tuple[0]
  temperature = data_tuple[1]
  pressure = data_tuple[2]
  humidity = data_tuple[3]
  solrad = data_tuple[4]
  """
  --- Start debugging:
  np.set_printoptions(precision=3)
  print("data_lbls   : ", np.size(data_lbls), data_lbls[-5:])
  print(" ")
  print("temperatuur : ", np.size(temperature), temperature[-5:])
  print(" ")
  print("vochtigheid : ", np.size(humidity), humidity[-5:])
  print("luchtdruk   : ", np.size(pressure), pressure[-5:])
  print("zon.straling: ", np.size(solrad), solrad[-5:])
  print(" ")
  --- End debugging.
  """
  # Set the bar width
  bar_width = 0.75
  # Set the color alpha
  ahpla = 0.7
  # positions of the left bar-boundaries
  tick_pos = list(range(1, len(data_lbls) + 1))

  # *** TEMPERATURE / HUMIDITY
  plt.rc('font', size=13)
  dummy, ax1 = plt.subplots(1, figsize=(20, 5))

  # Create a line plot of temperature/humidity
  ax1.plot(tick_pos, temperature,
           label='Temperatuur',
           alpha=ahpla,
           color='red',
           marker='o'
           )

  # Set Axes stuff
  ax1.set_ylabel("[degC]")
  ax1.set_xlabel("Datetime")
  ax1.grid(which='major', axis='y', color='k', linestyle='--', linewidth=0.5)
  ax1.legend(loc='upper left', framealpha=0.2)

  # Set plot stuff
  plt.xticks(tick_pos, data_lbls, rotation=-60)
  plt.title(f'{plot_title}')

  ax2 = ax1.twinx()
  ax2.plot(tick_pos, humidity,
           label='Vochtigheid',
           alpha=ahpla,
           color='blue',
           marker='o'
           )
  ax2.set_ylabel("[%]")
  ax2.legend(loc='upper right', framealpha=0.2)

  # Fit every nicely
  plt.xlim([min(tick_pos) - bar_width, max(tick_pos) + bar_width])
  plt.tight_layout()
  plt.savefig(fname=f'{output_file}TH.png', format='png')

  # *** PRESSURE
  plt.rc('font', size=13)
  dummy, ax3 = plt.subplots(1, figsize=(20, 5))

  # Create a bar plot of pressure
  ax3.bar(tick_pos, pressure,
          width=bar_width,
          label='Pressure',
          alpha=ahpla,
          color='green',
          align='center',
          bottom=0
          )

  # Set Axes stuff
  ax3.set_ylabel("[mbara]")
  ax3.set_xlabel("Datetime")
  ax3.grid(which='major', axis='y', color='k', linestyle='--', linewidth=0.5)
  ax3.legend(loc='upper left', framealpha=0.2)

  # Set plot stuff
  plt.xticks(tick_pos, data_lbls, rotation=-60)
  # plt.title(f'{plot_title}')

  # Fit every nicely
  plt.xlim([min(tick_pos) - bar_width, max(tick_pos) + bar_width])
  plt.ylim([min(pressure)-10, max(pressure)+10])
  plt.tight_layout()
  plt.savefig(fname=f'{output_file}P.png', format='png')

  # *** SOLAR RADIATION
  plt.rc('font', size=13)
  dummy, ax4 = plt.subplots(1, figsize=(20, 5))

  # Create a bar plot of solar radiation
  ax4.bar(tick_pos, solrad,
          width=bar_width,
          label='Solar Radiation',
          alpha=ahpla,
          color='orange',
          align='center',
          bottom=0
          )

  # Set Axes stuff
  ax4.set_ylabel(r"[$\sum kW/m^2$]")
  ax4.set_xlabel("Datetime")
  ax4.grid(which='major', axis='y', color='k', linestyle='--', linewidth=0.5)
  ax4.legend(loc='upper left', framealpha=0.2)

  # Set plot stuff
  plt.xticks(tick_pos, data_lbls, rotation=-60)
  # plt.title(f'{plot_title}')

  # Fit every nicely
  plt.xlim([min(tick_pos) - bar_width, max(tick_pos) + bar_width])
  plt.ylim([0, max(solrad)])
  plt.tight_layout()
  plt.savefig(fname=f'{output_file}S.png', format='png')


def main():
  """
    This is the main loop
    """
  OPTION = kl.get_cli_params(1)

  if OPTION in ['-d', '-D', '-a', '-A']:
    plot_graph('/tmp/domod/site/img/again_pastday_',
               fetch_last_day(),
               f"Trend afgelopen dagen ({dt.now().strftime('%d-%m-%Y %H:%M:%S')})"
               )

  if OPTION in ['-m', '-M', '-a', '-A']:
    plot_graph('/tmp/domod/site/img/again_pastmonth_',
               fetch_last_month(),
               f"Trend afgelopen maand ({dt.now().strftime('%d-%m-%Y %H:%M:%S')})"
               )


if __name__ == "__main__":
  main()
