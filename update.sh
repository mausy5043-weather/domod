#!/bin/bash

# update.sh is run periodically by a cronjob.
# * It synchronises the local copy of domod with the current GitLab branch
# * It checks the state of and (re-)starts daemons if they are not (yet) running.

HOSTNAME="$(hostname)"
branch="$(<"${HOME}/.domod.branch")"

# Wait for the daemons to finish their job. Prevents stale locks when restarting.
#echo "Waiting 30s..."
#sleep 30

# make sure working tree exists
if [ ! -d /tmp/domod/site/img ]; then
  mkdir -p /tmp/domod/site/img
  chmod -R 755 /tmp/domod
fi

pushd "${HOME}/domod" || exit 1
  # shellcheck disable=SC1091
  source ./includes
  git fetch origin
  # Check which files have changed
  DIFFLIST=$(git --no-pager diff --name-only "$branch..origin/${branch}")
  git pull
  git fetch origin
  git checkout "${branch}"
  git reset --hard "origin/${branch}" && git clean -f -d

  for fname in $DIFFLIST; do
    echo ">   ${fname} was updated from GIT"
    f5l4="${fname:0:13}${fname:${#fname}-4}"

    # Detect changes
    if [[ "${f5l4}" == "daemons/againd.py" ]]; then
      echo "  ! Domotica daemon changed"
      eval "./${fname} stop"
    fi

    #CONFIG.INI changed
    if [[ "${fname}" == "config.ini" ]]; then
      echo "  ! Configuration file changed"
      echo "  o Restarting all daemons"
      # shellcheck disable=SC2154
      for daemon in ${runlist}; do
        echo "  +- Restart again${daemon}"
        eval "./daemons/again${daemon}d.py restart"
      done
    fi
  done

  # Check if daemons are running
  # shellcheck disable=SC2154
  for daemon in ${runlist}; do
    if [ -e "/tmp/domod/${daemon}.pid" ]; then
      if ! kill -0 "$(<"/tmp/domod/${daemon}.pid")" >/dev/null 2>&1; then
        logger -p user.err -t domod "  * Stale daemon ${daemon} pid-file found."
        rm "/tmp/domod/${daemon}.pid"
        echo "  * Start again${daemon}"
        eval "./daemons/again${daemon}d.py start"
      fi
    else
      logger -p user.warn -t domod "Found again${daemon} not running."
      echo "  * Start again${daemon}"
      eval "./daemons/again${daemon}d.py start"
    fi
  done

  scripts/upload.sh --all

popd || exit
