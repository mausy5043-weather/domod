#!/usr/bin/env python3

"""
Communicate with the buienradar website to fetch weather data from selected stations

- temperature
- humidity
- pressure
- solar radiation

Store the data from the website of buienradar.nl in an sqlite3 database.
"""

import configparser
import datetime as dt
import json
import os
import sqlite3
import sys
import syslog
import time
import traceback
import urllib.request

# noinspection PyUnresolvedReferences
import mausy5043funcs.fileops3 as mf
# noinspection PyUnresolvedReferences
from mausy5043libs.libdaemon3 import Daemon

# constants
DEBUG = False
IS_JOURNALD = os.path.isfile('/bin/journalctl')
MYID = "".join(list(filter(str.isdigit, os.path.realpath(__file__).split('/')[-1])))
MYAPP = os.path.realpath(__file__).split('/')[-3]
NODE = os.uname()[1]

STATIONS = [6340, 6350, 6356, 6370, 6375]
STN_DIST = [14.0, 1.5, 9.0, 9.5, 12.5]
T_MEMORY = [[]] * len(STATIONS)
H_MEMORY = [[]] * len(STATIONS)
P_MEMORY = [[]] * len(STATIONS)
S_MEMORY = [[]] * len(STATIONS)

URL = "https://api.buienradar.nl/data/public/1.1/jsonfeed"


# noinspection PyUnresolvedReferences
class MyDaemon(Daemon):
  """Override Daemon-class run() function."""

  # pylint: disable=too-few-public-methods

  # noinspection PyUnresolvedReferences
  @staticmethod
  def run():
    """Execute main loop."""
    iniconf = configparser.ConfigParser()
    iniconf.read(f"{os.environ['HOME']}/{MYAPP}/config.ini")

    report_time = iniconf.getint(MYID, "reporttime")
    fdatabase = f"{os.environ['HOME']}/{iniconf.get(MYID, 'databasefile')}"
    sqlcmd = iniconf.get(MYID, 'sqlcmd')
    sample_time = report_time / iniconf.getint(MYID, 'samplespercycle')

    test_db_connection(fdatabase)

    while True:
      try:
        start_time = time.time()

        do_work(fdatabase, sqlcmd)

        pause_time = (sample_time
                      - (time.time() - start_time)
                      - (start_time % sample_time))
        if pause_time > 0:
          mf.syslog_trace(f"Waiting  : {pause_time}s", False, DEBUG)
          mf.syslog_trace("................................", False, DEBUG)
          time.sleep(pause_time)
        else:
          mf.syslog_trace(f"Behind   : {pause_time}s", False, DEBUG)
          mf.syslog_trace("................................", False, DEBUG)
      except Exception:
        mf.syslog_trace("Unexpected error in run()", syslog.LOG_CRIT, DEBUG)
        mf.syslog_trace(traceback.format_exc(), syslog.LOG_CRIT, DEBUG)
        raise


def do_work(receiving_db, commit_command):
  """Process the data from the website"""
  global T_MEMORY
  global H_MEMORY
  global P_MEMORY
  global S_MEMORY
  global STATIONS
  dt_format = '%m/%d/%Y %H:%M:%S'
  new_dt_format = '%Y-%m-%d %H:%M:%S'
  arr_temperature = T_MEMORY
  arr_humidity = H_MEMORY
  arr_pressure = P_MEMORY
  arr_solrad = S_MEMORY
  date_time = ''
  epoch = 0

  station_data = query_website()

  for stn in station_data:
    stn_id = int(stn['@id'])
    if stn_id in STATIONS:
      mf.syslog_trace(f"Station name : {stn['stationnaam']}", False, DEBUG)
      stn_idx = STATIONS.index(stn_id)
      # recall previous values
      temperature = arr_temperature[stn_idx]
      humidity = arr_humidity[stn_idx]
      pressure = arr_pressure[stn_idx]
      solrad = arr_solrad[stn_idx]
      for key in stn:
        if key == 'temperatuurGC':
          temperature = stn[key].strip()
          if temperature == '-':
            temperature = None
        if key == 'luchtvochtigheid':
          humidity = stn[key].strip()
          if humidity == '-':
            humidity = None
        if key == 'luchtdruk':
          pressure = stn[key].strip()
          if pressure == '-':
            pressure = None
        if key == 'zonintensiteitWM2':
          solrad = stn[key].strip()
          if solrad == '-':
            # -100 is used to denote an invalid or unknown datapoint.
            solrad = None
        if key == 'datum':
          epoch = int(dt.datetime.strptime(stn[key].strip(), dt_format).timestamp())
          date_time = time.strftime(new_dt_format, time.localtime(epoch))
      # store new values
      arr_temperature[stn_idx] = temperature
      arr_humidity[stn_idx] = humidity
      arr_pressure[stn_idx] = pressure
      arr_solrad[stn_idx] = solrad
      # push to DB
      datablock = [date_time, epoch, stn_id, temperature, humidity, pressure, solrad]
      mf.syslog_trace(f"Station data : {datablock}", False, DEBUG)
      do_add_to_database(datablock, receiving_db, commit_command)

  datablock = [date_time,
               epoch,
               1,
               f"{guesstimate_local_value(arr_temperature):.1f}",
               f"{guesstimate_local_value(arr_humidity):.1f}",
               f"{guesstimate_local_value(arr_pressure):.1f}",
               f"{guesstimate_local_value(arr_solrad):.1f}"
               ]

  mf.syslog_trace(f"Local data : {datablock}", False, DEBUG)
  do_add_to_database(datablock, receiving_db, commit_command)

  T_MEMORY = arr_temperature
  H_MEMORY = arr_humidity
  P_MEMORY = arr_pressure
  S_MEMORY = arr_solrad

  return


def guesstimate_local_value(data_source):
  """
  Guess the local value of a parameter based on the known values for various
  stations and given the distance to each station.
  """
  global STN_DIST
  station_inverse = list()
  data_array = list()
  # '-100.0' is used to denote an invalid or unknown datapoint.
  # remove '-100.0' values from the data_array and the same index from station_inverse
  for idx, x in enumerate(data_source):
    if x not in ['-100.0', None]:
      station_inverse.append(1 / STN_DIST[idx])
      data_array.append(float(x))

  # determine normalisation factor for each remaining station
  station_weight = [x / sum(station_inverse) for x in station_inverse]
  # determine normalised value
  normalised_data = [float(int(station_weight[i] * x * 10))/10 for i, x in enumerate(data_array)]
  # guess the local value
  guessed_value = sum(normalised_data)
  # mf.syslog_trace(f"Guess : {guessed_value}", False, DEBUG)
  return guessed_value


# noinspection PyUnresolvedReferences
def query_website():
  """Fetch current weather from website."""
  global URL
  retries = 4       # number of retries before giving up
  retry_pause = 31  # seconds to wait before retrying
  stns = {}

  while True:
    try:
      response = urllib.request.urlopen(URL)
      data = json.loads(response.read())
      # only return current station info
      stns = data['buienradarnl']['weergegevens']['actueel_weer']['weerstations']['weerstation']
    except (urllib.error.URLError, json.JSONDecodeError):
      retries -= 1
      if retries:
        time.sleep(retry_pause)
        continue
      else:
        stns = {}
        break
    break

  return stns


def do_add_to_database(results, fdatabase, sql_cmd):
  """Commit the results to the database."""
  # Get the time and date in human-readable form and UN*X-epoch...
  conn = None
  cursor = None
  # we don't record the datetime of addition to the database here.
  # instead we use the datetime we got from buienradar.
  # So, we can just dump the data into sqlite3.
  mf.syslog_trace(f"   @: {results[0]}", False, DEBUG)
  mf.syslog_trace(f"    : {results[1:]}", False, DEBUG)

  err_flag = True
  while err_flag:
    try:
      conn = create_db_connection(fdatabase)
      cursor = conn.cursor()
      if not epoch_is_present_in_database(cursor, results[1], results[2]):
        mf.syslog_trace(f"   @: {results[0]} = {results[3]} {results[4]} {results[5]} {results[6]}", False, DEBUG)
        cursor.execute(sql_cmd, results)
        cursor.close()
        conn.commit()
        conn.close()
      else:
        mf.syslog_trace(f"Skip: {results[0]}", False, DEBUG)
      err_flag = False
    except sqlite3.OperationalError:
      if cursor:
        cursor.close()
      if conn:
        conn.close()


def epoch_is_present_in_database(db_cur, epoch, stn_id):
  """
  Test if results is already present in the database
  :param db_cur: object database-cursor
  :param epoch: int
  :return: boolean  (true if data is present in the database for the given site at or after the given epoch)
  """
  db_cur.execute(f"SELECT MAX(sample_epoch) FROM weather WHERE stn_id = {stn_id};")
  db_epoch = db_cur.fetchone()[0]
  if db_epoch:
    if db_epoch >= epoch:
      return True
  return False


def create_db_connection(database_file):
  """
  Create a database connection to the SQLite3 database specified by database_file.
  """
  consql = None
  mf.syslog_trace(f"Connecting to: {database_file}", False, DEBUG)
  try:
    consql = sqlite3.connect(database_file, timeout=9000)
    # if consql:    # dB initialised succesfully -> get a cursor on the dB and run a test.
    #  cursql = consql.cursor()
    #  cursql.execute("SELECT sqlite_version()")
    #  versql = cursql.fetchone()
    #  cursql.close()
    #  logtext = f"Attached to SQLite3 server : {versql}"
    #  syslog.syslog(syslog.LOG_INFO, logtext)
    return consql
  except sqlite3.Error:
    mf.syslog_trace("Unexpected SQLite3 error when connecting to server.", syslog.LOG_CRIT, DEBUG)
    mf.syslog_trace(traceback.format_exc(), syslog.LOG_CRIT, DEBUG)
    if consql:  # attempt to close connection to SQLite3 server
      consql.close()
      mf.syslog_trace(" ** Closed SQLite3 connection. **", syslog.LOG_CRIT, DEBUG)
    raise


def test_db_connection(fdatabase):
  """
  Test & log database engine connection.
  """
  try:
    conn = create_db_connection(fdatabase)
    cursor = conn.cursor()
    cursor.execute("SELECT sqlite_version();")
    versql = cursor.fetchone()
    cursor.close()
    conn.commit()
    conn.close()
    syslog.syslog(syslog.LOG_INFO, f"Attached to SQLite3 server: {versql}")
  except sqlite3.Error:
    mf.syslog_trace("Unexpected SQLite3 error during test.", syslog.LOG_CRIT, DEBUG)
    mf.syslog_trace(traceback.format_exc(), syslog.LOG_CRIT, DEBUG)
    raise


if __name__ == "__main__":
  daemon = MyDaemon(f'/tmp/{MYAPP}/{MYID}.pid')  # pylint: disable=C0103

  # initialise logging
  syslog.openlog(ident=MYAPP, facility=syslog.LOG_LOCAL0)

  if len(sys.argv) == 2:
    if sys.argv[1] == 'start':
      daemon.start()
    elif sys.argv[1] == 'stop':
      daemon.stop()
    elif sys.argv[1] == 'restart':
      daemon.restart()
    elif sys.argv[1] == 'debug':
      # assist with debugging.
      print("Debug-mode started. Use <Ctrl>+C to stop.")
      DEBUG = True
      mf.syslog_trace("Daemon logging is ON", syslog.LOG_DEBUG, DEBUG)
      daemon.run()
    else:
      print("Unknown command")
      sys.exit(2)
    sys.exit(0)
  else:
    print("usage: {0!s} start|stop|restart|debug".format(sys.argv[0]))
    sys.exit(2)
