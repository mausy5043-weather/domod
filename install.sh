#!/bin/bash

# this repo gets installed either manually by the user or automatically by
# a `*boot` repo.

ME=$(whoami)
required_commonlibversion="0.6.0"
commonlibbranch="v0_6"

echo -n "Started installing DOMOd on "
date
minit=$(echo $RANDOM/555 | bc)
echo "MINIT = ${minit}"

install_package() {
  # See if packages are installed and install them.
  package=$1
  echo "*********************************************************"
  echo "* Requesting ${package}"
  status=$(dpkg-query -W -f='${Status} ${Version}\n' "${package}" 2>/dev/null | wc -l)
  if [ "${status}" -eq 0 ]; then
    echo "* Installing ${package}"
    echo "*********************************************************"
    sudo apt-get -yuV install "${package}"
  else
    echo "* Already installed !!!"
    echo "*********************************************************"
  fi
}

getfilefromserver() {
  file="${1}"
  mode="${2}"

  #if [ ! -f "${HOME}/${file}" ]; then
  cp -rvf "${HOME}/bin/.config/home/${file}" "${HOME}/"
  chmod "${mode}" "${HOME}/${file}"
  #fi
}

sudo apt-get update
install_package "lftp"

# Python 3 package and associates
install_package "build-essential"
install_package "python3"
install_package "python3-dev"
install_package "python3-pip"

# Support for matplotlib & numpy
install_package "libatlas-base-dev"

# gnuPlot packages
#install_package "python-numpy"
#install_package "gnuplot"
#install_package "gnuplot-nox"

# SQLite3 support (incl python3)
install_package "sqlite3"

echo
echo "*********************************************************"
sudo python3 -m pip install --upgrade pip setuptools wheel
sudo python3 -m pip install -r requirements.txt

getfilefromserver ".my.airco.cnf" "0740"

#legacy: commonlibversion=$(pip3 freeze | grep mausy5043 | cut -c 26-)
commonlibversion=$(python3 -m pip freeze | grep mausy5043 | cut -c 26-)
if [ "${commonlibversion}" != "${required_commonlibversion}" ]; then
  echo
  echo "*********************************************************"
  echo "Install common python functions..."
  #legacy: sudo pip3 uninstall -y mausy5043-common-python
  sudo python3 -m  pip uninstall -y mausy5043-common-python
  pushd /tmp || exit 1
    git clone -b "${commonlibbranch}" https://gitlab.com/mausy5043-installer/mausy5043-common-python.git
    pushd /tmp/mausy5043-common-python || exit 1
      #legacy sudo ./setup.py install
      sudo python3 -m setup.py install
    popd || exit
    sudo rm -rf mausy5043-common-python/
  popd || exit
  echo
  echo -n "Installed: "
  #legacy pip3 freeze | grep mausy5043
  sudo python3 -m pip freeze | grep mausy5043
  echo
fi

pushd "${HOME}/domod" || exit 1
  # To suppress git detecting changes by chmod:
  git config core.fileMode false
  # set the branch
  if [ ! -e "${HOME}/.domod.branch" ]; then
    echo "newweather" >"${HOME}/.domod.branch"
  fi

  # Recover the database from the server
  ./scripts/bakrecdb.sh --install

  # Create the /etc/cron.d directory if it doesn't exist
  sudo mkdir -p /etc/cron.d
  # Set up some cronjobs
  echo "# m h dom mon dow user  command" | sudo tee /etc/cron.d/domod
  echo "${minit}  * *   *   *   ${ME}    sleep 80; ${HOME}/domod/scripts/bakrecdb.sh --backup 2>&1 | logger -p info -t domod" | sudo tee --append /etc/cron.d/domod
  echo "*/10  * *   *   *   ${ME}    sleep 61; ${HOME}/domod/scripts/pastday.sh 2>&1 | logger -p info -t domod" | sudo tee --append /etc/cron.d/domod
  echo "01  * *   *   *   ${ME}    sleep 12; ${HOME}/domod/scripts/pastmonth.sh 2>&1 | logger -p info -t domod" | sudo tee --append /etc/cron.d/domod
  # echo "03  01 *   *   *   ${ME}    sleep 12; ${HOME}/domod/scripts/pastyear.sh 2>&1 | logger -p info -t domod" | sudo tee --append /etc/cron.d/domod
  # echo "13  01 *   *   *   ${ME}    sleep 12; ${HOME}/domod/scripts/vsyear.sh 2>&1 | logger -p info -t domod" | sudo tee --append /etc/cron.d/domod
  # echo "23  01 *   *   *   ${ME}    sleep 12; ${HOME}/domod/scripts/vsmonth.sh 2>&1 | logger -p info -t domod" | sudo tee --append /etc/cron.d/domod
  # @reboot we allow for 10s for the network to come up:
  echo "@reboot             ${ME}    sleep 10; ${HOME}/domod/update.sh 2>&1 | logger -p info -t domod" | sudo tee --append /etc/cron.d/domod
  # TODO: enable after testing:
  # echo "*/59 *  *  *   *   ${ME}    sleep 10; ${HOME}/domod/update.sh 2>&1 | logger -p info -t domod" | sudo tee --append /etc/cron.d/domod
  echo "59 04  *  *   *   ${ME}    sleep 10; ${HOME}/domod/update.sh 2>&1 | logger -p info -t domod" | sudo tee --append /etc/cron.d/domod

popd || exit

echo
echo "*********************************************************"
echo -n "Finished installation of DOMOd on "
date
